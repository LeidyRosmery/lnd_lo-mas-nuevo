const path = require('path');

const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

/**
 * Rutas
 */
const PATHS = {
    src: path.join(__dirname, 'src'),
    build: path.resolve(__dirname, 'production')
}

/**
 * Archivos y rutas de entrada
 */
const entry = {
    main: `${PATHS.src}/index.js`
}

/**
 * Archivos y rutas de salida
 */
const output = {
    filename: '[name].[hash:20].js',
    path: PATHS.build,
    publicPath: '/static/categorias/contenidoEstatico/landings/Lo-mas-nuevo/3.0/'
}

const node = {
    fs: 'empty'
}

/**
 * Configuración de los módulos
 */
const moduleConfig = {
    rules: [
        {
            test: /\.js$/,
            exclude: [/node_modules/, /src\/js\//],
            loader: 'babel-loader',

            options: {
                presets: ['env']
            }
        },
        {
            test: /\.(scss|css|sass)$/,
            use: [
                { loader: MiniCssExtractPlugin.loader },
                {
                    // translates CSS into CommonJS
                    loader: 'css-loader',
                    options: {
                        sourceMap: true
                    }
                },
                {
                    // Runs compiled CSS through postcss for vendor prefixing
                    loader: 'postcss-loader',
                    options: {
                        sourceMap: true
                    }
                },
                {
                    // compiles Sass to CSS
                    loader: 'sass-loader',
                    options: {
                        outputStyle: 'expanded',
                        sourceMap: true,
                        sourceMapContents: true
                    }
                }
            ]
        },
        {
            // Load all images as base64 encoding if they are smaller than 8192 bytes
            test: /\.(png|jpg|gif|svg)$/,
            use: [{
                loader: 'url-loader',
                options: {
                    name: '[name].[hash:20].[ext]',
                    limit: 8192
                }
            }]
        }
    ]
}

/**
 * Configuración de la opción minify de HtmlWebpackPlugin
 */
const htmlMinify = {
    collapseWhitespace: true,
    minifyJS: true,
    minifyCSS: true,
    removeEmptyAttributes: true,
    removeComments: true
}

/**
 * Retorna una instancia de HtmlWebpackPlugin
 * de acuerdo al template y nombre de archivo que reciba de parámetro
 * 
 * @param {*} template 
 * @param {*} filename 
 */
const getHtmlWebpackPluginConfig = ( template, filename ) => {
    return new HtmlWebpackPlugin({
        template: template,
        filename: filename,
        inject: false,
        minify: htmlMinify
    })
}

const plugins = [ 
  
    getHtmlWebpackPluginConfig( './index.html', 'index.html' ),

    new CleanWebpackPlugin(PATHS.build),
    /*        new FaviconsWebpackPlugin({
        // Your source logo
        logo: './src/assets/icon.png',
        // The prefix for all image files (might be a folder or a name)
        prefix: 'icons-[hash]/',
        // Generate a cache file with control hashes and
        // don't rebuild the favicons until those hashes change
        persistentCache: true,
        // Inject the html into the html-webpack-plugin
        inject: true,
        // favicon background color (see https://github.com/haydenbleasel/favicons#usage)
        background: '#fff',
        // favicon app title (see https://github.com/haydenbleasel/favicons#usage)
        title: 'registration-contest}}',

        // which icons should be generated (see https://github.com/haydenbleasel/favicons#usage)
        icons: {
            android: true,
            appleIcon: true,
            appleStartup: true,
            coast: false,
            favicons: true,
            firefox: true,
            opengraph: false,
            twitter: false,
            yandex: false,
            windows: false
        }
    }), */

    /**
     * Minifica los archivos css
     */
    new MiniCssExtractPlugin({
        filename: '[name].[contenthash].css',
        chunkFilename: '[id].[name].[contenthash].css'
    }),

    /**
     * Optimiza el procesamiento de css
     */
    new OptimizeCssAssetsPlugin({
        cssProcessor: require('cssnano'),
        cssProcessorOptions: {
            map: {
                inline: false,
            },
            discardComments: {
                removeAll: true
            }
        },
        canPrint: true
    }),

    new MomentLocalesPlugin({
        localesToKeep: ['es-us', 'es'],
    }),

    /**
     * Remueve css que no se usa
     */
    //new PurgecssPlugin({
      //  paths: glob.sync(`${PATHS.src}/**/*`,  { nodir: true }),
    //})
]

/**
 * Configuración de 'optimization' de webpack 
 */
const optimization = {
    minimizer: [
        new UglifyJsPlugin({
            //test: /\.js(\?.*)?$/i,
            sourceMap: true,
            //exclude: [ /node_modules/ ],
            uglifyOptions: {
                mangle: true,
                compress: {
                    drop_console: true,
                    pure_funcs: [ 'console.log' ]
                },
                output: {
                    comments: false,
                },
            },
        })
    ],
    namedModules: true,
    namedChunks: true,
    splitChunks: {
        cacheGroups: {
            vendor: {
                test: /[\\/]node_modules[\\/]((?!(moment)).*)[\\/]/,
                name: 'vendor',
                chunks: 'all',
                reuseExistingChunk: true,
            },
            moment: {
                test: /[\\/]node_modules[\\/](moment)[\\/]/,
                name: 'moment',
                chunks: 'all',
                reuseExistingChunk: true,
            }
        }
    }
}

module.exports = {
    devtool: 'source-map',
    entry: entry,
    output: output,
    node: node,
    module: moduleConfig,
    plugins: plugins,
    optimization: optimization
};