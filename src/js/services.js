import { getCook } from './utils'

// Try to get price zone from cookie or default
function getCurrentPrizeZone() {
  const zoneCookie = getCook('comuna')
  return zoneCookie ? zoneCookie : '150136'
}

/**
 * Obtiene los productos desde la API de productos de Sodimac
 * 
 * @param {string} codes // cadena de skus separados por comas ej: '1234567-957485-544115...'
 * @returns {Promise}
 */
export const getAPIProducts = (codes) => {
    return new Promise((resolve) => {
        if (typeof $ === "undefined") {
            throw new Error('jQuery not loaded')
        }

        /**
         * Devuelve una respuesta falsa para consultas en ambiente de desarrollo
         */
        if (process.env.NODE_ENV === "development") {
            const db = require('./fake-prod-db')
            const codesArr = codes.split('-')
            const fakeReponse = []
         
            for (let f = 0; f < codesArr.length; f++) {
                if (db[codesArr[f]]) {
                   
                    fakeReponse.push(db[codesArr[f]])
                }
            }
    
            resolve(fakeReponse)
        } else {            
            $.ajax({
                url: `/s/search/v1/sope/product-details?productId=${codes}&priceGroup=2&zone=${getCurrentPrizeZone()}&currentpage=1`,
                
            }).done((res, textStatus) => {
                if (textStatus === "success") {
                    // console.log("claro" , res)
                    resolve(res.productDetailsJson)
                }
            }).fail(() => {
                console.error('Error al consultar la BD corp de productos.')
                // reject()
            })
        }
    })
}

/**
 * Obtiene una hoja de googlesheets en formato JSON
 * 
 * @param {*} url 
 * @returns {Promise}
 */
export const getGSheetsJSON = (url, options = {}) => {
    if (typeof $ === "undefined") {
        throw new Error('jQuery not loaded')
    }

    return new Promise((resolve, reject) => {
        if (typeof url !== "undefined") {
           let ajaxOptions = Object.assign({
            url: url,
           }, options)

           $.ajax(ajaxOptions).done((res, textStatus) => {
              if (textStatus === "success") {              
                 resolve(res.values)
              }else{
                  console.log("no conecta")
              }
           }).fail((res) => {
              reject(res.responseText)
           })
        } else {
           reject('Error: No se especificó la url para la extracción de datos')
        }
     })
}