// sliderjs section daylis
import Swiper from 'swiper/dist/js/swiper';

new Swiper("#swiper-categories", {
    slidesPerView: 6,
    spaceBetween: 10,
    slidesPerGroup: 3,
    autoplay: {
        delay: 6500
    },
    // loop:true,
    preventClicks: false,
    preventClicksPropagation: false,
    preloadImages: false,
    lazy: {
        preloaderClass: 'spinner-loading',
    },
    pagination: {
        el: ".swiper-categorias-pagination",
        clickable: true,
        type: 'bullets'
    },   
    allowTouchMove: true,   
    navigation: {
        nextEl: ".swiper-slider-categorias-next",
        prevEl: ".swiper-slider-categorias-prev"
        
    },
    observer: true,
    breakpoints: {
       // when window width is <= 480px
        480: {
            slidesPerView: 2,
            slidesPerGroup: 2,
        },
        // when window width is <= 680px
        640: {
            slidesPerView: 3,
            // slidesPerGroup: 3,
        },
        // when window width is <= 768px
        768: {
            slidesPerView: 3,
            // slidesPerGroup: 3,
        },
           // when window width is <= 992px
          1024: {
            slidesPerView: 4,
            // slidesPerGroup: 3,
        }
    }
});
new Swiper("#swiper-brands", {
    slidesPerView: 4,
    spaceBetween: 10,
    slidesPerGroup: 2,
    autoplay: {
        delay: 6500
    },
    // loop:true,
    preventClicks: false,
    preventClicksPropagation: false,
    preloadImages: false,
    lazy: {
        preloaderClass: 'spinner-loading',
    },
    pagination: {
        el: ".swiper-marcas-pagination",
        clickable: true,
        type: 'bullets'
    },   
    allowTouchMove: true,   
    navigation: {
        nextEl: ".swiper-slider-marcas-next",
        prevEl: ".swiper-slider-marcas-prev"
        
    },
    observer: true,
    breakpoints: {
       // when window width is <= 480px
        480: {
            slidesPerView: 2,
            slidesPerGroup: 2,
        },
        // when window width is <= 680px
        640: {
            slidesPerView: 3,
            // slidesPerGroup: 3,
        },
        // when window width is <= 768px
        768: {
            slidesPerView: 3,
            // slidesPerGroup: 3,
        },
           // when window width is <= 992px
           992: {
            slidesPerView: 3,
            // slidesPerGroup: 3,
        }
    }
});





