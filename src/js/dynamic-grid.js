import moment from 'moment'



import {
   hasWebpSupport ,
   isIOS ,
   getPriceParsed,
   getArrayChunks,
   slugify
} from './utils'
import { getAPIProducts, getGSheetsJSON } from './services'


/**
 * Campos válidos solo para hojas con productos o categorías
 * que serán renderizados como tarjetas
 */



const imgWebpParam = hasWebpSupport() && !isIOS() ? '&fmt=webp&qlt=90' : ''

const PRODCARD_CODE = 0
const PRODCARD_PRICE_PROMO = 1
const PRODCARD_PRICE_REGULAR = 2
const PRODCARD_IS_CMR = 3
const PRODCARD_IS_COMBO = 4
const PRODCARD_CID = 5
const PRODCARD_IS_ACTIVE = 6

/**
 * Default breakpoints
 */
const bpMobile = 480
const bpTablet = 768


// sliderjs section produc-panel
class DynamiGrid {
   constructor(selector, options) {
      const containerEl = document.querySelector(selector)

      if (!selector || !containerEl) return false;
      this.selector = selector
      this.containerEl = containerEl
      this.carouselClass = `${selector.replace('#', '')}-carousel`
      this.prodData = {}
      this.colSize = {}

      this.options = Object.assign({}, options)

      this.init();

   }
   init() {

      this.setMountPointEl(document.querySelector(this.selector))
      this.getData().then(data => this.renderHTML(data));
      
    

   }

  
   getHTMLItem(dataItem) {

      if (typeof dataItem === "undefined")
         return;


   }

   setMountPointEl(mountPointEl) {
      this.mountPointEl = mountPointEl
   }
   getData() {
      return new Promise((resolve, reject) => {
         getGSheetsJSON(`https://sheets.googleapis.com/v4/spreadsheets/${this.options.sheetKey}/values/${this.options.sheetIndex}!A:G?key=AIzaSyCy7KgzNIBGtO5YjyyKoXi2BROJ1EtDRgQ`)
            .then(data => resolve(data))
            .catch(err => reject(err))
      })
   }

   // js-slider
   initCarousel() {
      let carouselOptions = {
         spaceBetween: this.options.hasProdCards ? 3 : 8,
         autoplay: {
            delay: 6500,
         },
         preventClicks: false,
         preventClicksPropagation: false,
         allowTouchMove: true,
         slidesPerView: this.options.slidesPerviewOnly ? 3 : 3,
         pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
         },
         breakpoints: {
            [bpTablet]: {
               slidesPerView: 3,
            },
            [bpTablet - 150]: {
               slidesPerView: 1,
            },
            [bpMobile]: {
               slidesPerView: 1,
               spaceBetween: this.options.hasProdCards ? 5 : 8,
            }
         }
         //breakpoints: configBreakpoints
      }

      // Enable carousel pagination
      if (this.options.carouselPagination) {
         carouselOptions.pagination = {
            el: `.${this.carouselClass}__pagination`,
            clickable: true
         }
      }

      // Enable carousel navigation 
      if (this.options.carouselNav) {
         carouselOptions.navigation = {
            nextEl: `.${this.carouselClass}__outer .swiper-button-next`,
            prevEl: `.${this.carouselClass}__outer .swiper-button-prev`
         }
      }

      import('swiper/dist/js/swiper')
         .then(Swiper => {
            this.swiperPanel = new Swiper.default(`.${this.carouselClass}`, carouselOptions)
         })
   }

   /**
    * Obtiene solo la data requerida de la fila
    * 
    * @param {*} data 
    */
   getSheetProdCardData(data) {

      const code = this.isFieldValid(data[PRODCARD_CODE]) ? data[PRODCARD_CODE].replace(/-/, '').replace(/\s/, '').trim() : null
    
      const cid = this.isFieldValid(data[PRODCARD_CID]) ? data[PRODCARD_CID] : null
 
     


      return {
         code: code,
         pricePromo: this.isFieldValid(data[PRODCARD_PRICE_PROMO]) ? data[PRODCARD_PRICE_PROMO] : null,
         priceRegular: this.isFieldValid(data[PRODCARD_PRICE_REGULAR]) ? data[PRODCARD_PRICE_REGULAR] : null,
         isCMR: this.isFieldValid(data[PRODCARD_IS_CMR]) ? parseInt(data[PRODCARD_IS_CMR]) : null,
         isCombo: this.isFieldValid(data[PRODCARD_IS_COMBO]) ? parseInt(data[PRODCARD_IS_COMBO]) : null,
         isActive: this.isFieldValid(data[PRODCARD_IS_ACTIVE]) ? parseInt(data[PRODCARD_IS_ACTIVE]) : null,
         cid: this.isFieldValid(data[PRODCARD_CID]) ? data[PRODCARD_CID] : null,
     
       
      }
   }

   /**
    * Verifica la vigencia de precio según rango de fechas
    * 
    * @param {*} dateStart 
    * @param {*} dateEnd 
    */
 

   /**
    * Obtiene los precios de producto (CMR | EVENTO | NORMAL)
    * 
    * @param {*} { MT2, EVENTOMT2, CMRMT2, CMR, CMRFrom, CMRValidity, NORMAL, EVENTO, EVENTFrom, EVENTOValidity }
    */
   getPrice({ MT2, EVENTOMT2, INTERNET, INTERNETFrom, AB, ABFrom, ABValidity, INTERNETValidity, CMR, CMRMT2, CMRFrom, CMRValidity, NORMAL, EVENTO, EVENTOValidity, EVENT, EVENTFrom, EVENTValidity }) {

      if (CMR ) {
         if (CMRMT2 && MT2) {
            return {
               PROMO: CMRMT2,
               REGULAR: CMRMT2 < MT2 ? MT2 : null
            }
         }
         return {
            PROMO: CMR,
            REGULAR: CMR < NORMAL ? NORMAL : null
         }
      } else if (EVENT) {
         return {
            PROMO: EVENT,
            REGULAR: EVENT < NORMAL ? NORMAL : null
         }
      } else {
         if (INTERNET ) {

            return {
               PROMO: INTERNET,
               REGULAR: INTERNET < NORMAL ? NORMAL : null,
               INTERNET: true
            }
         }
         else {
            if (EVENTO ) {
               if (EVENTOMT2) {
                  return {
                     PROMO: EVENTOMT2,
                     REGULAR: EVENTOMT2 < MT2 ? MT2 : null
                  }
               }
               return {
                  PROMO: EVENTO,
                  REGULAR: EVENTO < NORMAL ? NORMAL : null
               }
            } else {
               if (AB) {

                  return {
                     PROMO: AB,
                     REGULAR: AB < NORMAL ? NORMAL : null,
                     MASBAJO: true
                  }
               }
            }
         }

      }
      return {
         PROMO: NORMAL
      }
   }
   /**
    * Verifica si el producto es CMR
    * 
    * @param {*} param
    * @returns {Boolean}
    */
   getIsCMR({ CMR }) {

      return CMR
   }

   /**
    * Combina la data del excel con la de la API
    * 
    * @param {Array} data
    */
   mergeProdItems(data) {
      if (data && data.length) {
         for (let d = 0; d < data.length; d++) {
           

               const code = data[d].productId
               const sheetProd = this.prodData[code] ? this.prodData[code] : null
               if (sheetProd) {
          
                  const description = data[d].name
                  const marca =  data[d].brand
                  const pricePromo = sheetProd.pricePromo ? parseFloat(sheetProd.pricePromo) : this.getPrice(data[d]).PROMO
                  const priceRegular = sheetProd.priceRegular ? parseFloat(sheetProd.priceRegular) : this.getPrice(data[d]).REGULAR
                  const isCMR = sheetProd.isCMR !== null ? sheetProd.isCMR : this.getIsCMR(data[d])
                  const isCombo = sheetProd.isCombo !== null ? sheetProd.isCombo : data[d].combo
                  const prodURL = `/sodimac-pe/product/${code}/${slugify(description)}${sheetProd.cid ? `/?cid=${sheetProd.cid}` : ''}`
                  const prodImg=`https://sodimac.scene7.com/is/image/SodimacPeru/${code}?$lista155$${imgWebpParam}&qlt=90`
                  const hasMT2Price = typeof data[d].MT2 !== "undefined" && data[d].MT2 > 0
                  const status = data[d].status
       
                  Object.assign(this.prodData[code], {
                     description: description,
                     marca: marca,
                     status:status,
                     pricePromo: pricePromo,
                     priceRegular: priceRegular,
                     isCMR: isCMR,
                     isINTERNET: this.getPrice(data[d]).INTERNET !== "undefined" & !isCombo ? this.getPrice(data[d]).INTERNET : false,
                     isMASBAJOS: this.getPrice(data[d]).MASBAJO !== "undefined" ? this.getPrice(data[d]).MASBAJO : false,
                     isCombo: isCombo,
                     prodURL: prodURL,
                     prodImg: prodImg,
                     hasMT2Price: hasMT2Price
                  })
               }
            
         }
      }
   }

   /**
    * Realiza el cruce de datos del excel con la API Corp.
    * 
    * @param {*} sheetData 
    */
   getCrossSheetDataWithAPI(sheetData) {

      if (typeof sheetData === "undefined") {
         return
      }

      const codes = []
      const apiReqLimit = 10

      for (let c = 0; c < sheetData.length; c++) {
         const item = sheetData[c]
         const code = this.isFieldValid(item[PRODCARD_CODE]) ? item[PRODCARD_CODE].replace(/-/, '').replace(/\s/, '').trim() : null
    
         const isActive = this.isFieldValid(item[PRODCARD_IS_ACTIVE]) ? item[PRODCARD_IS_ACTIVE] : null

         if (code ) {

            // Verificar si el item está activado
            if (isActive == 1) {
               this.prodData[code] = Object.assign(this.getSheetProdCardData(item), { position: c })

               // Almacenar los skus si el item es de tipo 'prod'
             
                  codes.push(code)
               
            }
         }
      }

      this.totalProducts = codes.length

      // Realiza la petición de productos
      // tomando en cuenta el límite de items de respuesta.
      if (this.totalProducts > apiReqLimit) {
         const chunkCodes = getArrayChunks(codes, apiReqLimit)
         return new Promise((resolve, reject) => {

            const reqPromises = chunkCodes.map(codesArr => {

               if (codesArr.length) {
                  return getAPIProducts(codesArr.join('-'))
               }
            })

            return Promise.all(reqPromises)
               .then(res => {
              
                  if (res.length) {
                     for (let i = 0; i < res.length; i++) {
                        this.mergeProdItems(res[i])

                     }
                  }

                  resolve(this.prodData)
               })
         })
      } else {
         return new Promise((resolve, reject) => {
            codes.length &&
               getAPIProducts(codes.join('-'))
                  .then(res => {
              
                     this.mergeProdItems(res)
                     resolve(this.prodData)
                  })
                  .catch(() => {
                     reject('Algo salió mal al hacer la llamada a la API.')
                  })
         })
      }
   }

   /**
    * Render HTML products data
    * @param {Promise} data 
    */
   renderHTMLProdData(data) {
console.log("Mathias" , data)
      data.then((_data) => {
         const dataArr = Object.keys(_data).map(code => {
            return _data[code]
         }).sort((item1, item2) => {
            if (item1.position > item2.position) {
               return 1;
            }

            if (item1.position < item2.position) {
               return -1;
            }

            return 0;
         })


         if (this.options.enableCarousel) {

            this.renderCarousel(dataArr)
            this.initCarousel()
         } else {
      
            this.renderColumn(dataArr)

         }

      })
   }





   renderHTML(sheetData) {

      if (typeof sheetData === "undefined" || (sheetData && !sheetData.length)) {
         this.containerEl.parentNode.parentNode.style.display = 'none'
      }

      let data = this.options.hasProdCards ? this.getCrossSheetDataWithAPI(sheetData) : sheetData

      if (this.options.carouselMobileOnly) {
         this.options.enableCarousel = (this.isTablet || this.isMobile)
      }

      if (this.options.hasProdCards && data instanceof Promise) {
         this.renderHTMLProdData(data)
      } else {
         if (this.options.enableCarousel) {
            this.renderHTMLProdData(data)
            // this.renderCarousel(data)
            // this.initCarousel()
         } else {
            this.renderColumn(data)
         }
      }


   }

   renderCarousel(data) {
      if (typeof data !== "undefined") {
         let innerHTML = `<div style="width:100%;" class="${this.carouselClass}__outer content-daily swiper-container-outer"><div class="swiper-container ${this.carouselClass} ${this.options.hasProdCards ? `dynamic-grid-carousel__cards` : ''} dynamic-grid-carousel"><div class="swiper-wrapper">`

         if (data.length > 0) {
            this.totalItems = data.length
            data.map(dataItem => {
               innerHTML += (this.options.hasProdCards ? this.getHTMLProdCard(dataItem) : this.getHTMLItem(dataItem))
            })
         }
         innerHTML += `</div> </div>
            ${this.options.carouselPagination ? `<div class="${this.carouselClass}__pagination dynamic-grid-carousel__pagination swiper-pagination"></div>` : ''}
           

         </div>`

         if (this.mountPointEl) {
            this.mountPointEl.innerHTML = innerHTML
         }
      }
   }

   getHTMLProdCard({ hasMT2Price, marca, description, pricePromo, status, priceRegular, isCMR, isINTERNET, isMASBAJOS, isCombo, prodURL, prodImg, catImg, catURL, itemType, savingsPerc }) {
      console.log("regulat", this.options.enableCarousel)
      const itemTemplate = status === 'OK' ?
         `<div  class="${this.options.enableCarousel ? ' swiper-slide dynamic-grid-carousel__item dynamic-grid-carousel__item__prodcard' : `column  dynamic-grid__column__card`}">
         <div class="daily-deals-container no-margin">
            <a ${description ? `title="${description}"` : ''} class="ddeals-link no-decoration color-dark" href="${prodURL}">
            <div class="wrapper-card" >

               <div class="ddeals-content-image text-center">
                  <img ${description ? `alt="${description}"` : ''} class="ddeals-image" src="${prodImg}">
               </div>
               <div class="ddeals-content-info" >
               <span class="ddeals-promo-badge ${isCMR ? 'is-cmr' : ''} ${isINTERNET ? 'is-internet' : ''} ${isMASBAJOS ? 'is-mas-bajos' : ''} ${isCombo ? 'is-combo' : ''}">
               <span class="ddeals-promo-badge-cmr"></span>
               <span class="ddeals-promo-badge-combo"></span>
               <span class="ddeals-promo-badge-internet"></span>
               <span class="ddeals-promo-badge-mas-bajos"></span>
            </span>
                  <div class="ddeals-wrapper">
                  <span class="ddeals-brand block">${marca}</span>
                     <span class="ddeals-name block">${description}</span>
                  </div>
                  <div class="ddeals-wrapper_price">
               
              

               

                  ${pricePromo ? `
                     <div class="bold    color-white inline ddeals-price-promo-wrap">
                        <span class="signo">S/</span>
                        <span class="ddeals-price-promo">${getPriceParsed(pricePromo)}${hasMT2Price ? '<span class="ddeals-mt2-text"> / m<sup>2</sup></span>' : ''}</span>
                     </div>` : ''}
                  ${priceRegular !== null && priceRegular > 0 && priceRegular !== pricePromo ?
            `<div class="ddeals-price-secondary">
                           <span class="ddeals-price-label">Antes: </span>
                           <span>S/</span>
                           <span class="ddeals-price-list">${getPriceParsed(priceRegular)}${hasMT2Price ? '<span class="ddeals-mt2-text"> / m<sup>2</sup></span>' : ''}</span>
                        </div>` :
            ''
         }

     
             </div></div>
               </div>
            </a>
         </div>
      </div>` :
         ``

      return itemTemplate
   }

   renderColumn(data) {
      console.log("imprime", data)
      if (typeof data !== "undefined") {
         let innerHTML = `<div class="columns is-mobile ${this.options.hasProdCards ? 'dynamic-grid__card-columns content-daily' : ''}">`

         if (data.length > 0) {
            this.totalItems = data.length

            data.map((dataItem, indice) => {
               this.getHTMLItem(dataItem)
               dataItem = Object.assign({
                
                  indicePosicion: indice
               }, dataItem)


               innerHTML += (this.options.hasProdCards ? this.getHTMLProdCard(dataItem) : this.getHTMLItem(dataItem))
            })
         }

         innerHTML += '</div>'

         if (this.mountPointEl) {
            this.mountPointEl.innerHTML = innerHTML
         }
      }
   }

   isFieldValid(field) {
      if (typeof field !== "undefined" && field !== "") {
         return typeof field !== "undefined" && typeof field !== null
      }
      return null
   }
}

export default DynamiGrid
