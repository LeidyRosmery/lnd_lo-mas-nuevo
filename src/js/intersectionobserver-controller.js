/**
 * Controlador para Observar intersección sobre elementos del DOM
 * 
 * @param {*} target 
 * @param {*} options 
 * @param {*} onIntersecting 
 */
export default function IObserverController(target, options, onIntersecting) {
   this.targetEl = target instanceof HTMLElement ? target : document.querySelector(target)

   const buildThresholdList = (numSteps) => {
      var thresholds = [];
      var numSteps = numSteps;

      for (var i = 1.0; i <= numSteps; i++) {
         var ratio = i / numSteps;
         thresholds.push(ratio);
      }

      thresholds.push(0);
      return thresholds;
   }

   this.options = Object.assign({
      root: null,
      rootMargin: '20px',
      thresholds: buildThresholdList(20)
   }, options)

   this.observer = new IntersectionObserver((entries, observer) => {
      entries.forEach(entry => {
         if (entry.isIntersecting) {
            if (typeof onIntersecting === "function") {
               onIntersecting()
            }
         }
      })
   }, this.options)

   this.targetEl && this.observer.observe(this.targetEl)
}