import MutationObserver from 'mutation-observer'

export default function MutationObserverHandler(target, options) {
   let _options = Object.assign(options, {
      config: {
         attributes: true,
         childList: true,
         characterData: true
      }
   })

   const observer = new MutationObserver((mutations) => {
      mutations.map(mutation => {
         typeof _options.onMutate === "function" && _options.onMutate(mutation)
      })
   })

   try {
      target && observer.observe(target, _options.config)
   } catch(err) {
      console.err(err)
   }
}
 