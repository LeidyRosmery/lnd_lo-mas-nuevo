export default function MatchMediaHandler(media, options) {
   if (!media) return false;

   var _self = this;
   _self.options = Object.assign({}, options);
   _self.media = media;
   _self.list = window.matchMedia(media);

   if (_self.list.matches) {
      if (typeof _self.options.onMatch === "function") {
         _self.options.onMatch()
      }
   }

   attachListener()

   function attachListener() {
      _self.list.addListener(listenerHandler);
   }

   function listenerHandler(mql) {
      if (mql.matches) {
         if (typeof _self.options.onMatch === "function") {
            _self.options.onMatch()
         }
      }
   }
}
