import { hasWebpSupport } from './utils'

// Lazy load images to improve page load performace
export default function LazyImages() {
    const imageElems = document.querySelectorAll('.b-lazy')
    const getURLWithFmt = (url) => {
       if (url.indexOf('?') !== -1) {
          return hasWebpSupport() ? `${url}&fmt=webp` : url
       }
       return hasWebpSupport() ? '?fmt=webp' : ''
    }
    if (imageElems.length > 0) {
       for (let i = 0; i < imageElems.length; i++) {
          if (!imageElems[i].classList.contains('b-loaded')) {
             if (imageElems[i].dataset.srcset) {
                imageElems[i].setAttribute('srcset', `${getURLWithFmt(imageElems[i].dataset.srcset)}`)
                //delete imageElems[i].dataset.srcset
             }
             if (imageElems[i].dataset.src) {
                imageElems[i].src = getURLWithFmt(imageElems[i].dataset.src)
                //delete imageElems[i].dataset.src
             }
             imageElems[i].classList.add('b-loaded')
          }
       }
       if (typeof picturefill === "function") {
          picturefill()
       }
    }
 }