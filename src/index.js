/* Style imports  */
//import 'normalize.css/normalize.css';
import './styles/vendor.scss';
import './styles/core.scss';
import './js/sliders'


/**
 * Script module imports
 */
import {
   isTouchMode,
   hasWebpSupport,
   isIOS,
   isIE
} from './js/utils'


import LazyImages from './js/lazy-images'


/**
 * KEY de la hoja de excel principal de contenido
 */
const MAIN_SPREADSHEET_KEY = '1mX6YuqpNHRfA7ihTUXRIw2GyoHZUpHvrL4P3TxGLWWA'

/**
 * Requerir todos los polyfills necesarios
 */
const loadPolyfills = () => {

   /**
    * Verificar soporte del objecto 'Promise'
    * importar polyfill si no hay soporte
    */
   if (typeof window.Promise !== "function") {
      import('promise-polyfill')
         .then(PromisePolyfill => {
            window.Promise = PromisePolyfill.default
         })
   }

   /**
    * Verificar soporte del objecto 'IntersectionObserver',
    * importar polyfill si no hay soporte
    */
   if (typeof window.IntersectionObserver !== "function") {
      import('intersection-observer')
   }

   /**
    * Verificar soporte del objecto 'requestAnimationFrame',
    * importar polyfill si no hay soporte
    */
   if (typeof window.requestAnimationFrame === "undefined") {
      import('./js/raf')
   }
}

/**
 * Agrega clases de detección al elemento 'html'
 */
const addHTMLClasses = () => {

   // Add touch mode class
   if (isTouchMode()) {
      document.documentElement.classList.add('touch-mode')
   }

   if (hasWebpSupport()) {
      document.documentElement.classList.add('supports-webp')
   }

   if (isIOS()) {
      document.documentElement.classList.add('ios')
   }

   if (isIE()) {
      document.documentElement.classList.add('ie')
   }
}




/**
 * Carga la sección que se le pase de forma diferida (lazyload)
 * 
 * @param {*} target 
 * @param {*} callback 
 */
const lazyLoadSection = (target, callback) => {
   const targetEl = target instanceof HTMLElement ? target : document.querySelector(target)
   import(/* webpackChunkName: "intersectionobserver-controller" */`./js/intersectionobserver-controller`)
     .then(IObserverController => {
         new IObserverController.default(targetEl, { rootMargin: '175px' }, () => {
            // Verificar si la sección ya se cargó
            if (targetEl && targetEl.className.indexOf('js-section-loaded') === -1) {
               setTimeout(() => {
                  targetEl.classList.add('js-section-loaded')
               }, 350)
               callback()
            }
         })
      })
}



;(function (d, $) {
   $(function () {

      /**
       * Add referential and support body classes
       */
      addHTMLClasses()

      /**
       * Cargar los polyfills necesarios
       */
      loadPolyfills();

      lazyLoadSection('#js-dg-recommended', () => {
         import(/* webpackChunkName: "dynamic-grid" */ `./js/dynamic-grid`)
            .then(DynamicGrid => {
               new DynamicGrid.default('#js-dg-recommended', {
                  sheetIndex: "Lo-Mas-Recomendado",
                  sheetKey: MAIN_SPREADSHEET_KEY,
                  enableCarousel: true,
                  carouselNav: false,
                  hasProdCards: true,
                  carouselMobileOnly: false
               })
            })
      })

      lazyLoadSection('#js-dg-products', () => {
         import(/* webpackChunkName: "dynamic-grid" */ `./js/dynamic-grid`)
            .then(DynamicGrid => {
               new DynamicGrid.default('#js-dg-products', {
                  sheetIndex: "Nuevos-Productos",
                  sheetKey: MAIN_SPREADSHEET_KEY,
                  enableCarousel: false,
                  carouselNav: false,
                  hasProdCards: true,
                  carouselMobileOnly: false
               })
            })
      })

    
 
       
   })

 
   /**
    * Inicializa la carga de imágenes de forma diferida (lazy load)
    */

   new LazyImages()


   /**
    * Manejador de dirección de scroll (scroll up / scroll down)
    */
   // handleScrollDirection()

   /**
    * Activar visibilidad de la wincha
    */
   d.body.classList.add('showTopBanner')

   /**
    * Agregar clase al body para definir si la Preventa está en curso
    */
   d.body.classList.add('is-presale')



})(document, jQuery);


