const path = require('path');
const glob = require('glob');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const PurgecssPlugin = require('purgecss-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

module.exports = {
    devtool: 'eval-cheap-module-source-map',
    entry: {
        main: './src/index.js'
    },
    devServer: {
        host: 'localhost',
        open: 'chrome',
        port: 8082,
        contentBase: path.join(__dirname, "development")
    },
    node: {
        fs: 'empty'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: [/node_modules/, /src\/js\//],
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            },
            {
                test: /\.(scss|css)$/,
                use: [
                    {
                        // creates style nodes from JS strings
                        loader: "style-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // translates CSS into CommonJS
                        loader: "css-loader",
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        // compiles Sass to CSS
                        loader: "sass-loader",
                        options: {
                            outputStyle: 'expanded',
                            sourceMap: true,
                            sourceMapContents: true
                        }
                    }
                    // Please note we are not running postcss here
                ]
            }
            ,
            {
                // Load all images as base64 encoding if they are smaller than 8192 bytes
                test: /\.(png|jpg|gif|svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            // On development we want to see where the file is coming from, hence we preserve the [path]
                            name: '[path][name].[ext]?hash=[hash:20]',
                            limit: 8192
                        }
                    }
                ],
            },
            {
                // Load JSON/XML data
                test: /\.(json|xml)$/,
                use: [
                    {
                        loader: 'json-loader',
                        options: {
                            name: '[path][name].[ext]?hash=[hash:20]'
                        }
                    }
                ]
            }
        ],
    },
    
    plugins: [
        new PurgecssPlugin({
          paths: glob.sync(`${path.join(__dirname, 'src')}/**/*`,  { nodir: true }),
        }),
        new HtmlWebpackPlugin({
            template: './index.html',
            inject: false,
            title: 'Cyber Sodimac',
            unsupportedBrowser: true,
        }),
        new MomentLocalesPlugin({
            localesToKeep: ['es-us', 'es'],
        }),
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]((?!(moment)).*)[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                    reuseExistingChunk: true,
                },
                moment: {
                    test: /[\\/]node_modules[\\/](moment)[\\/]/,
                    name: 'moment',
                    chunks: 'all',
                    reuseExistingChunk: true,
                }
            }
        }
    }
};
